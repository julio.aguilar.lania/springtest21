package com.az.testspring.rest;

import com.az.testspring.dao.TemasRepository;
import com.az.testspring.models.Tema;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SimpleController {

    TemasRepository temasRepo;

    public SimpleController(TemasRepository temasRepo) {
        this.temasRepo = temasRepo;
    }

    @GetMapping("/hola")
    public String saludar() {
        return "Hola mundo";
    }

    @GetMapping("/temas")
    public List<Tema> getTemas() {
        return temasRepo.getTodos();
    }
}
