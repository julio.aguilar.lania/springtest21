package com.az.testspring.dao;

import com.az.testspring.models.Tema;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.IntStream;

@Repository
public class TemasRepository {
    public List<Tema> getTodos() {
        return IntStream.rangeClosed(1,10)
                .mapToObj(k -> Tema.randomTema())
                .toList();
    }
}
