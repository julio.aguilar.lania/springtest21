package com.az.testspring.models;

@SuppressWarnings("unused")
public class Tema {
    String titulo;
    Integer horas;

    public Tema(String titulo, Integer horas) {
        this.titulo = titulo;
        this.horas = horas;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Integer getHoras() {
        return horas;
    }

    public void setHoras(Integer horas) {
        this.horas = horas;
    }

    private static final String[] TITULOS = {
            "Biología","Computación","Español", "Física",
            "Historia","Inglés","Matemáticas", "Música","Química"
    };
    public static Tema randomTema() {
        String titulo = TITULOS[(int) Math.floor(Math.random()*TITULOS.length)];
        int numSerie = (int) Math.floor(Math.random()*1000);
        titulo += " " + numSerie;
        int horas = (int) Math.floor(Math.random()*10)*4;
        return new Tema(titulo, horas);
    }
}
