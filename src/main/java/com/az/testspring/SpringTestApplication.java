package com.az.testspring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTestApplication {

    private final static Logger LOGGER = LoggerFactory.getLogger(SpringTestApplication.class);

    public static void main(String[] args) {
        LOGGER.debug("Application start");
        SpringApplication.run(SpringTestApplication.class, args);
    }

}